package view;

import model.Response;
import model.domain.Product;
import model.domain.Total;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import static helper.LoadJsonFromFile.loadJsonFromFile;
import static org.junit.jupiter.api.Assertions.assertEquals;

class JsonOutputFormatterTest {
    private final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    private static final String TEST_FILE = "src/test/resources/sampleAppOutput.json";
    private final PrintStream printStreamOutput = System.out;
    private JsonOutputFormatter jsonOutputFormatter;
    private Response response;

    @BeforeEach
    public void setUpStream() {
        System.setOut(new PrintStream(byteArrayOutputStream));
        jsonOutputFormatter = new JsonOutputFormatter();
        response = Response.builder()
                .results(buildProductList())
                .total(buildTotal())
                .build();
    }

    @AfterEach
    public void restoreStream() {
        System.setOut(printStreamOutput);
    }

    @Test
    void shouldPrintResults() throws IOException {
        jsonOutputFormatter.printResults(response);
        String expectedOutput = loadJsonFromFile(TEST_FILE).replaceAll("\\s", "");
        assertEquals(expectedOutput, byteArrayOutputStream.toString().replaceAll("\\s", ""));
    }

    @Test
    void shouldPrintMessageWhenResultsAreEmpty() {
        jsonOutputFormatter.printResults(null);
        assertEquals("Nothing retrieved from website :(\n", byteArrayOutputStream.toString());
    }

    private List<Product> buildProductList() {
        List<Product> products = new LinkedList<>();
        products.add(Product.builder()
                .title("Sainsbury's Blueberries 200g")
                .calories(33)
                .unitPrice(new BigDecimal("1.75"))
                .description("by Sainsbury's blueberries")
                .build());
        return products;
    }

    private Total buildTotal() {
        Total total = new Total();
        total.setGross(new BigDecimal("5.00"));
        total.setVat(0.83);
        return total;
    }
}
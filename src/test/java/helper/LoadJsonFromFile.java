package helper;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class LoadJsonFromFile {

    public static String loadJsonFromFile(String file) throws IOException {
        Path path = Paths.get(file);
        StringBuilder json = new StringBuilder();
        List<String> lines = Files.readAllLines(path);
        for (String line : lines) {
            json.append(line);
        }
        return json.toString();
    }
}

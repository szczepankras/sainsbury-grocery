package service;

import service.error.UnableToLoadPageException;
import model.Response;
import model.domain.Total;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ProductServiceTest {

    private static final String URL = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html";

    @Test
    void getProductsSummary() throws UnableToLoadPageException {
        ProductService productService = new ProductService(URL);

        Response response = productService.getProductsSummary();

        Total expectedTotal = new Total();
        expectedTotal.setGross(new BigDecimal("39.50"));
        expectedTotal.setVat(6.58);
        assertNotNull(response);
        assertEquals(17, response.getResults().size());
        assertEquals(expectedTotal, response.getTotal());
    }
}
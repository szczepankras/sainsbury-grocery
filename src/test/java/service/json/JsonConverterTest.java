package service.json;

import model.Response;
import model.domain.Product;
import model.domain.Total;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import static helper.LoadJsonFromFile.loadJsonFromFile;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static service.json.JsonConverter.convertToJson;

class JsonConverterTest {

    private Response response;
    private String expectedResponse;
    private static final String SAMPLE_TEST_JSON_RESPONSE = "src/test/resources/sampleTest.json";
    private static final String SAMPLE_TEST_JSON_RESPONSE_WITHOUT_CALORIES = "src/test/resources/sampleTestWithoutCalories.json";

    @BeforeEach
    void setUp() throws IOException {
        List<Product> products = new LinkedList<>();
        products.add(Product.builder()
                .title("Sainsbury's Blueberries 200g")
                .calories(33)
                .unitPrice(new BigDecimal("1.25"))
                .description("by Sainsbury's blueberries")
                .build());

        products.add(Product.builder()
                .title("Sainsbury's Strawberries 150g")
                .calories(56)
                .unitPrice(new BigDecimal("3.75"))
                .description("by Sainsbury's strawberries")
                .build());

        Total total = new Total();

        total.setGross(new BigDecimal("5.00"));
        total.setVat(0.83);

        response = Response.builder()
                .results(products)
                .total(total)
                .build();

        expectedResponse = loadJsonFromFile(SAMPLE_TEST_JSON_RESPONSE).replaceAll("\\s", "");
    }

    @Test
    void shouldConvertToJson() {
        String result = convertToJson(response).replaceAll("\\s", "");
        assertEquals(expectedResponse, result);
    }

    @Test
    void shouldConvertToJsonWithoutCaloriesElement() throws IOException {
        List<Product> products = new LinkedList<>();
        products.add(Product.builder()
                .title("Sainsbury's Blueberries 200g")
                .calories(33)
                .unitPrice(new BigDecimal("1.25"))
                .description("by Sainsbury's blueberries")
                .build());

        products.add(Product.builder()
                .title("Sainsbury's Strawberries 150g")
                .unitPrice(new BigDecimal("3.75"))
                .description("by Sainsbury's strawberries")
                .build());

        Total total = new Total();

        total.setGross(new BigDecimal("5.00"));
        total.setVat(0.83);

        response = Response.builder()
                .results(products)
                .total(total)
                .build();

        String result = convertToJson(response).replaceAll("\\s", "");

        expectedResponse = loadJsonFromFile(SAMPLE_TEST_JSON_RESPONSE_WITHOUT_CALORIES).replaceAll("\\s", "");
        assertEquals(expectedResponse, result);
    }
}
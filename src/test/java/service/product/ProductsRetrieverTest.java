package service.product;

import service.error.UnableToLoadPageException;
import service.html.SainsburyHtmlExtractor;
import model.domain.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ProductsRetrieverTest {

    private SainsburyHtmlExtractor sainsburyHtmlExtractor = mock(SainsburyHtmlExtractor.class);
    List<String> calories = new LinkedList<>();
    List<String> titles = new LinkedList<>();
    List<String> prices = new LinkedList<>();
    List<String> descriptions = new LinkedList<>();

    @BeforeEach
    void setUp() throws UnableToLoadPageException {
        initData();
        when(sainsburyHtmlExtractor.extractProductTitles()).thenReturn(titles);
        when(sainsburyHtmlExtractor.extractCalories()).thenReturn(calories);
        when(sainsburyHtmlExtractor.extractPricesPerUnit()).thenReturn(prices);
        when(sainsburyHtmlExtractor.extractDescriptions()).thenReturn(descriptions);
    }

    @Test
    void shouldRetrieveProducts() throws UnableToLoadPageException {
        ProductsRetriever productsRetriever = new ProductsRetriever(sainsburyHtmlExtractor);

        List<Product> result = productsRetriever.getProducts();

        List<Product> expectedResult = new LinkedList<>();
        expectedResult.add(Product.builder()
                .title("Sainsbury's Blueberries 200g")
                .calories(33)
                .unitPrice(new BigDecimal("1.75"))
                .description("by Sainsbury's blueberries")
                .build());

        assertTrue(expectedResult.containsAll(result));

    }

    private void initData(){
        titles.add("Sainsbury's Blueberries 200g");
        calories.add("33kcal");
        prices.add("£1.75/unit");
        descriptions.add("by Sainsbury's blueberries");
    }

}
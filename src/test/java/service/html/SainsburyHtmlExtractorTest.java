package service.html;

import service.error.UnableToLoadPageException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class SainsburyHtmlExtractorTest {

    private SainsburyHtmlExtractor sainsburyHtmlExtractor;
    private static final String URL = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html";

    @BeforeEach
    void setUp() throws UnableToLoadPageException {
        DocumentHtmlLoader documentHtmlLoader = spy(new DocumentHtmlLoader(URL));
        when(documentHtmlLoader.getDocument()).thenCallRealMethod();
        sainsburyHtmlExtractor = new SainsburyHtmlExtractor(documentHtmlLoader);
    }

    @Test
    void shouldExtractProductTitles() throws UnableToLoadPageException {

        List<String> result = sainsburyHtmlExtractor.extractProductTitles();

        List<String> expectedResult = Arrays.asList("Sainsbury's Strawberries 400g",
                "Sainsbury's Blueberries 200g",
                "Sainsbury's Raspberries 225g",
                "Sainsbury's Blackberries, Sweet 150g",
                "Sainsbury's Blueberries 400g",
                "Sainsbury's Blueberries, SO Organic 150g",
                "Sainsbury's Raspberries, Taste the Difference 150g",
                "Sainsbury's Cherries 400g",
                "Sainsbury's Blackberries, Tangy 150g",
                "Sainsbury's Strawberries, Taste the Difference 300g",
                "Sainsbury's Cherry Punnet 200g",
                "Sainsbury's Mixed Berries 300g",
                "Sainsbury's Mixed Berry Twin Pack 200g",
                "Sainsbury's Redcurrants 150g",
                "Sainsbury's Cherry Punnet, Taste the Difference 200g",
                "Sainsbury's Blackcurrants 150g",
                "Sainsbury's British Cherry & Strawberry Pack 600g");

        assertEquals(expectedResult, result);
    }

    @Test
    void shouldExtractProductUnitPrices() throws UnableToLoadPageException {

        List<String> result = sainsburyHtmlExtractor.extractPricesPerUnit();

        List<String> expectedResult = Arrays.asList("£1.75/unit",
                "£1.75/unit",
                "£1.75/unit",
                "£1.50/unit",
                "£3.25/unit",
                "£2.00/unit",
                "£2.50/unit",
                "£2.50/unit",
                "£1.50/unit",
                "£2.50/unit",
                "£1.50/unit",
                "£3.50/unit",
                "£2.75/unit",
                "£2.50/unit",
                "£2.50/unit",
                "£1.75/unit",
                "£4.00/unit");

        assertEquals(expectedResult, result);
    }

    @Test
    void shouldExtractProductDescriptions() throws UnableToLoadPageException {

        List<String> result = sainsburyHtmlExtractor.extractDescriptions();

        List<String> expectedResult = Arrays.asList("by Sainsbury's strawberries",
                "by Sainsbury's blueberries",
                "by Sainsbury's raspberries",
                "by Sainsbury's blackberries",
                "by Sainsbury's blueberries",
                "So Organic blueberries Plump and Refreshing",
                "Ttd raspberries",
                "by Sainsbury's Family Cherry Punnet",
                "by Sainsbury's blackberries",
                "Ttd strawberries",
                "Description Cherries Succulent and sweet 1 of 5 a-day 10 cherries Class I",
                "by Sainsbury's mixed berries",
                "Description Mixed Berries 1 of 5 a-day 80g serving Class I Raspberries grown in Morocco Blueberries grown in Peru",
                "by Sainsbury's redcurrants",
                "Cherry Punnet",
                "Description Union Flag",
                "Description British Cherry & Strawberry Mixed Pack 1 of 5 a-day - 80g serving Class I Union Flag");

        assertEquals(expectedResult, result);
    }

    @Test
    void shouldExtractCalories() throws UnableToLoadPageException {

        List<String> result = sainsburyHtmlExtractor.extractCalories();

        List<String> expectedResult = Arrays.asList("33kcal",
                "45kcal",
                "32kcal",
                "32kcal",
                "45kcal",
                "45kcal",
                "32kcal",
                "52kcal",
                "32kcal",
                "33kcal",
                "52",
                "",
                "",
                "71kcal",
                "48kcal",
                "",
                "");

        assertEquals(expectedResult, result);
    }

    @Test
    void shouldThrowExceptionWhenDataCouldNotBeLoaded() throws UnableToLoadPageException {

        DocumentHtmlLoader documentHtmlLoader = mock(DocumentHtmlLoader.class);
        when(documentHtmlLoader.getDocument()).thenThrow(new UnableToLoadPageException());

        sainsburyHtmlExtractor = new SainsburyHtmlExtractor(documentHtmlLoader);

        Throwable error = assertThrows(UnableToLoadPageException.class, () -> sainsburyHtmlExtractor.extractDescriptions());
        assertThrows(UnableToLoadPageException.class, () -> sainsburyHtmlExtractor.extractProductTitles());
        assertThrows(UnableToLoadPageException.class, () -> sainsburyHtmlExtractor.extractDescriptions());
        assertThrows(UnableToLoadPageException.class, () -> sainsburyHtmlExtractor.extractCalories());

        assertEquals("Unable to reach given url. Please check your internet connection or website availability", error.getMessage());

    }

}
package service.html;

import service.error.UnableToLoadPageException;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DocumentHtmlLoaderTest {

    private static final String url = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html";

    @Test
    void shouldLoadDocument() throws UnableToLoadPageException {
        DocumentHtmlLoader documentHtmlLoader = new DocumentHtmlLoader(url);

        Document document = documentHtmlLoader.getDocument();

        assertNotNull(document);
    }

    @Test
    void shouldNotLoadDocumentFromNotExistingResource() {
        DocumentHtmlLoader documentHtmlLoader = new DocumentHtmlLoader("https://notexistingfake.gitb.com/");

        Throwable error = assertThrows(UnableToLoadPageException.class, () -> documentHtmlLoader.getDocument());

        assertEquals("Unable to reach given url. Please check your internet connection or website availability", error.getMessage());
    }

}
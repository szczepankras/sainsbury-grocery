package service.format;

import org.junit.jupiter.api.Test;

import static service.format.NumberFormatter.format;
import static org.junit.jupiter.api.Assertions.*;

class NumberFormatterTest {

    @Test
    void shouldFormatTextToNumber(){

        String numberToFormat = "23kcal";

        String result = format(numberToFormat);

        assertEquals("23", result);
    }

    @Test
    void shouldFormatTextWithDecimalToNumber(){

        String numberToFormat = "£1.50/unit";

        String result = format(numberToFormat);

        assertEquals("1.50", result);
    }

    @Test
    void shouldFormatEmptyTextWithToNumber(){

        String numberToFormat = "";

        String result = format(numberToFormat);

        assertNull(result);
    }

}
package controller;

import model.Response;
import service.ProductService;
import service.error.UnableToLoadPageException;
import view.JsonOutputFormatter;

public class ProductController {

    private final ProductService productService;
    private Response response;
    private JsonOutputFormatter jsonOutputFormatter;

    public ProductController(String url) {
        this.productService = new ProductService(url);
        this.jsonOutputFormatter = new JsonOutputFormatter();
    }

    public void loadDataFromWebsite() {
        try {
            response = productService.getProductsSummary();
        } catch (UnableToLoadPageException e) {
            System.out.println(e.getMessage());
        }
    }

    public void printResults(){
        jsonOutputFormatter.printResults(response);
    }
}

package service.format;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NumberFormatter {

    public static String format(String numberToFormat) {
        Pattern pattern = Pattern.compile("\\d.*\\d+");
        Matcher matcher = pattern.matcher(numberToFormat);
        String extractedValue = null;
        if(matcher.find()) {
            extractedValue = matcher.group();
        }
        return extractedValue;
    }
}

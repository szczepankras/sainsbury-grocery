package service.product;

import service.error.UnableToLoadPageException;
import service.html.SainsburyHtmlExtractor;
import lombok.AllArgsConstructor;
import model.domain.Product;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;
import java.util.List;

import static service.format.NumberFormatter.format;

@AllArgsConstructor
public class ProductsRetriever {

    private SainsburyHtmlExtractor sainsburyHtmlExtractor;

    public List<Product> getProducts() throws UnableToLoadPageException {

        List<String> calories = sainsburyHtmlExtractor.extractCalories();
        List<String> titles = sainsburyHtmlExtractor.extractProductTitles();
        List<String> prices = sainsburyHtmlExtractor.extractPricesPerUnit();
        List<String> descriptions = sainsburyHtmlExtractor.extractDescriptions();

        return buildProductList(titles, calories, prices, descriptions);
    }

    private List<Product> buildProductList(List<String> titles, List<String> calories, List<String> prices, List<String> descriptions) {
        List<Product> products = new LinkedList<>();
        for (int i = 0; i < titles.size(); i++) {
            products.add(Product.builder()
                    .title(titles.get(i))
                    .calories(calories.get(i).isEmpty() ? null : Integer.valueOf(format(calories.get(i))))
                    .unitPrice(new BigDecimal(format(prices.get(i))).setScale(2, RoundingMode.HALF_UP))
                    .description(descriptions.get(i))
                    .build());
        }
        return products;
    }
}

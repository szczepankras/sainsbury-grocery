package service.html;

import service.error.UnableToLoadPageException;
import lombok.AllArgsConstructor;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

@AllArgsConstructor
public class DocumentHtmlLoader {

    private String url;

    public Document getDocument() throws UnableToLoadPageException {
        try {
            return Jsoup.connect(url).get();
        } catch (IOException e) {
            throw new UnableToLoadPageException();
        }
    }
}

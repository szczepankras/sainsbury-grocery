package service.html;

import service.error.UnableToLoadPageException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class SainsburyHtmlExtractor {

    private static final int CALORIES_RECORD_NUMBER_IN_TABLE = 2;
    private static final int CALORIES_CELL_NUMBER_IN_RECORD_TABLE = 0;
    private static final String DUPLICATED_VALUE_SEPARATOR = " ";
    private static final String FIRST_LINE_FROM_DESCRIPTION_SEPARATOR = "\n";

    private DocumentHtmlLoader documentHtmlLoader;

    private List<String> titles = new LinkedList<>();
    private List<String> prices = new LinkedList<>();
    private List<String> descriptions = new LinkedList<>();
    private List<String> calories = new LinkedList<>();

    public SainsburyHtmlExtractor(DocumentHtmlLoader documentHtmlLoader) {
        this.documentHtmlLoader = documentHtmlLoader;
    }

    public List<String> extractProductTitles() throws UnableToLoadPageException {
        if (titles.isEmpty()) {
            loadData();
        }
        return titles;
    }

    public List<String> extractPricesPerUnit() throws UnableToLoadPageException {
        if (prices.isEmpty()) {
            loadData();
        }
        return prices;
    }

    public List<String> extractDescriptions() throws UnableToLoadPageException {
        if (descriptions.isEmpty()) {
            loadData();
        }
        return descriptions;
    }

    public List<String> extractCalories() throws UnableToLoadPageException {
        if (calories.isEmpty()) {
            loadData();
        }
        return calories;
    }

    private void loadData() throws UnableToLoadPageException {
        for (Element product : getProducts()) {
            titles.add(product.select("div.productNameAndPromotions").text());
            prices.add(product.getElementsByClass("pricePerUnit").text().split(DUPLICATED_VALUE_SEPARATOR)[0]);
            DocumentHtmlLoader documentHtmlLoader = new DocumentHtmlLoader(product.select("a").attr("abs:href"));
            Document documentOfSubPage = documentHtmlLoader.getDocument();
            descriptions.add(documentOfSubPage.getElementsByClass("productText").first().text().split(FIRST_LINE_FROM_DESCRIPTION_SEPARATOR)[0]);
            calories.add(getCaloriesCellValue(documentOfSubPage));
        }
    }

    private Elements getProducts() throws UnableToLoadPageException {
        Document document = documentHtmlLoader.getDocument();
        Elements products = document.select("div.product");
        return products;
    }

    private String getCaloriesCellValue(Document document) {
        String calories = "";
        Optional<Element> table = getTable(document);
        if (table.isPresent()) {
            calories = table
                    .get()
                    .select("tr")
                    .get(CALORIES_RECORD_NUMBER_IN_TABLE)
                    .select("td")
                    .get(CALORIES_CELL_NUMBER_IN_RECORD_TABLE)
                    .text();
        }
        return calories.split(DUPLICATED_VALUE_SEPARATOR)[0];
    }

    private Optional<Element> getTable(Document document) {
        return Optional.ofNullable(document
                .select("table")
                .first());
    }
}

package service;

import service.error.UnableToLoadPageException;
import service.html.DocumentHtmlLoader;
import service.html.SainsburyHtmlExtractor;
import service.product.ProductsRetriever;
import model.domain.Product;
import model.Response;
import model.domain.Total;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.List;

public class ProductService {

    private final ProductsRetriever productsRetriever;
    private static final double VAT_FACTOR = 1.2;

    public ProductService(String resourceUrl) {
        DocumentHtmlLoader documentHtmlLoader = new DocumentHtmlLoader(resourceUrl);
        SainsburyHtmlExtractor sainsburyHtmlExtractor = new SainsburyHtmlExtractor(documentHtmlLoader);
        productsRetriever = new ProductsRetriever(sainsburyHtmlExtractor);
    }

    public Response getProductsSummary() throws UnableToLoadPageException {
        List<Product> productList = productsRetriever.getProducts();
        return Response.builder()
                .results(productList)
                .total(calculateTotalSummary(productList))
                .build();
    }

    private Total calculateTotalSummary(List<Product> productList) {
        Total total = new Total();
        BigDecimal gross = new BigDecimal(BigInteger.ZERO);
        for (Product product : productList) {
            gross = gross.add(product.getUnitPrice());
        }
        total.setGross(gross.setScale(2, RoundingMode.HALF_UP));
        double VAT = gross.doubleValue() - gross.doubleValue() / VAT_FACTOR;
        total.setVat(roundDoubleValue(VAT));
        return total;
    }

    private double roundDoubleValue(double value){
      return (double)Math.round(value * 100) / 100;
    }
}

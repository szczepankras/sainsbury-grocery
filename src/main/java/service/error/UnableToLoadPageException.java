package service.error;

public class UnableToLoadPageException extends Exception {
    private static final String ERROR_MESSAGE = "Unable to reach given url. Please check your internet connection or website availability";

    public UnableToLoadPageException() {
        super(ERROR_MESSAGE);
    }
}

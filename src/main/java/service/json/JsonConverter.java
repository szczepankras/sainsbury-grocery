package service.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import model.Response;

public class JsonConverter {
    public static String convertToJson(Response data) {
        Gson gson = new GsonBuilder()
                .disableHtmlEscaping()
                .create();

        String json = gson.toJson(data);
        return json;
    }
}

package view;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import model.Response;

import static service.json.JsonConverter.convertToJson;

public class JsonOutputFormatter {

    public void printResults(Response response) {
        if (response == null) {
            System.out.println("Nothing retrieved from website :(");
        } else {
            String json = convertToJson(response);
            System.out.println(toPrettyFormat(json));
        }
    }

    private static String toPrettyFormat(String jsonString) {
        JsonParser parser = new JsonParser();
        JsonObject json = parser.parse(jsonString).getAsJsonObject();

        Gson gson = new GsonBuilder()
                .disableHtmlEscaping()
                .setPrettyPrinting()
                .create();

        String prettyJson = gson.toJson(json);

        return prettyJson;
    }
}

package model.domain;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.math.BigDecimal;

@Builder
@EqualsAndHashCode
public class Product {
    private String title;
    @SerializedName("kcal_per_100g")
    private Integer calories;
    @Getter
    @SerializedName("unit_price")
    private BigDecimal unitPrice;
    private String description;
}

package model.domain;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Total {

    private BigDecimal gross;
    private double vat;
}

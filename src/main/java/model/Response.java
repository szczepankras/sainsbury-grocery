package model;

import lombok.Builder;
import lombok.Getter;
import model.domain.Product;
import model.domain.Total;

import java.util.List;

@Builder
public class Response {

    @Getter
    private List<Product> results;
    @Getter
    private Total total;

}

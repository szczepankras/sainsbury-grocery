import controller.ProductController;
/*
       @author Szczepan Kras
 */
public class Application {
    private static final String SAINSBURY_WEBSITE = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html";

    public static void main(String[] args) {
        ProductController productController = new ProductController(SAINSBURY_WEBSITE);
        productController.loadDataFromWebsite();
        productController.printResults();
    }
}

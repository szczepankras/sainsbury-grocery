**How to run:**

- Just start main method in the "Application" class :)
- No input arguments are required
- URL to Sainsbury's website is provided in the code


**Requirements:**

- All requirements provided in the challenge are implemented


**Tests:**

- Logic of all components is covered by unit tests
- ProductServiceTest and SainsburyHtmlExtractorTest are rather functional tests for avoiding complex mocked structures


**Error handling:**

- UnableToLoadPageException - returns when application is not able to achieve website to scrape


**Tech Stack used:**

- Maven
- Java 10
- Junit 5
- Mockito
- Gson 
- Jsoup
- Lombok (for IntelliJ IDE -> Lombok plugin and Annotation processing enabled in the settings are required)
